﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using unirest_net.http;
using System.Net;
using Newtonsoft.Json.Linq;

namespace sql1
{
    class Program
    {
        static int tableWidth = 225;

        static void PrintLine()
        {
            Console.WriteLine(new string('-', tableWidth));
        }

        static void PrintRow(params string[] columns)
        {
            int width = (tableWidth - columns.Length) / columns.Length;
            string row = "|";

            foreach (string column in columns)
            {
                row += AlignCentre(column, width) + "|";
            }

            Console.WriteLine(row);
        }

        static string AlignCentre(string text, int width)
        {
            text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

            if (string.IsNullOrEmpty(text))
            {
                return new string(' ', width);
            }
            else
            {
                return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
            }
        }

        static void Main(string[] args)
        {
            var watch = new System.Diagnostics.Stopwatch();

            watch.Start();

            string connetionString = "Data Source=localhost;Initial Catalog=drustvena_mreza;User ID=root;Password=";
            MySqlConnection conn = new MySqlConnection(connetionString);
            MySqlCommand command = conn.CreateCommand();

            //dropDatabase(command);
            //createTables(command);
            //CREATE DATABASE drustvena_mreza

            //fillTables(connetionString, command);

            executeQueries(command);

            watch.Stop();

            Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds } ms");

            Console.WriteLine("Done hit key");
            Console.ReadKey();
        }

        private static void dropDatabase(MySqlCommand command)
        {
            command.CommandText = "DROP DATABASE drustvena_mreza";
            executeNonQuery(command);
        }

        public static void updateAndFillTables(string connetionString, MySqlCommand command) {
            //updateAndFillCountriesStatesCitiesTables(command);
            //updateAndFillUsersDeletedUsersClientsTables(connetionString, command);
            //updateAndFillFriendRequestsFriends(command);
            //updateAndFillLogTime(command);
        }

        public static void updateAndFillUsersDeletedUsersClientsTables(string connetionString, MySqlCommand command) {
            MySqlConnection conn = new MySqlConnection(connetionString);
            conn.Open();
            string queryUsers = "INSERT INTO users VALUES";
            string queryDeletedUsers = "INSERT INTO deleted_users VALUES";
            string queryClients = "INSERT INTO clients VALUES";

            for (int i = 0; i < 150; i++) {
                while (true)
                {
                    try
                    {
                        WebRequest request = WebRequest.Create("https://randomuser.me/api");
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        Stream dataStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        dynamic values = JObject.Parse(responseFromServer);

                        // Creates a TextInfo based on the "en-US" culture.
                        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                        // Changes a string to titlecase.
                        string city = values["results"][0]["location"]["city"];
                        city = textInfo.ToTitleCase(city);

                        string countryCode = values["results"][0]["nat"];
                        countryCode = countryCode.ToLower();

                        MySqlCommand commandCity = conn.CreateCommand();
                        commandCity.CommandText = "SELECT * FROM cities INNER JOIN states ON states.state_id = cities.state_id WHERE cities.name = \"" + city + "\" and states.country_code = \"" + countryCode + "\"";
                        //Console.WriteLine(commandCity.CommandText);

                        System.Data.DataSet ubb = executeSelect(commandCity);
                        if (ubb.Tables[0].Rows.Count != 0)
                        {

                            command.CommandText = "SELECT * FROM users WHERE users.username = \"" + values["results"][0]["login"]["username"] + "\"";
                            System.Data.DataSet ubb4 = executeSelect(command);
                            if (ubb4.Tables[0].Rows.Count != 0) {
                                i--;
                                break;
                            }

                            var homeCityId = ubb.Tables[0].Rows[0][0];

                            string firstName = values["results"][0]["name"]["first"];
                            firstName = firstName.First().ToString().ToUpper() + firstName.Substring(1);

                            string lastName = values["results"][0]["name"]["last"];
                            lastName = lastName.First().ToString().ToUpper() + lastName.Substring(1);

                            string birthday = values["results"][0]["dob"]["date"];
                            birthday = birthday.Split(' ')[0];
                            string[] birthdayData = birthday.Split('/');

                            bool gender = true;
                            if (values["results"][0]["gender"] == "female")
                            {
                                gender = false;
                            };

                            string dateCreated = values["results"][0]["registered"]["date"];
                            dateCreated = dateCreated.Split(' ')[0];
                            string[] dateCreatedData = dateCreated.Split('/');

                            Random gen = new Random();
                            DateTime date_updated = new DateTime(Int32.Parse(dateCreatedData[2]) + 1, 1, 1);
                            int range = (DateTime.Today - date_updated).Days;
                            date_updated = date_updated.AddDays(gen.Next(range));

                            while (true)
                            {
                                request = WebRequest.Create("https://randomuser.me/api");
                                response = (HttpWebResponse)request.GetResponse();
                                dataStream = response.GetResponseStream();
                                reader = new StreamReader(dataStream);
                                responseFromServer = reader.ReadToEnd();
                                dynamic values2 = JObject.Parse(responseFromServer);


                                string current_city = values2["results"][0]["location"]["city"];
                                current_city = textInfo.ToTitleCase(current_city);

                                string countryCode2 = values2["results"][0]["nat"];
                                countryCode2 = countryCode2.ToLower();

                                MySqlCommand commandCurrentCity = conn.CreateCommand();
                                commandCurrentCity.CommandText = "SELECT * FROM cities INNER JOIN states ON states.state_id = cities.state_id WHERE cities.name = \"" + current_city + "\" and states.country_code = \"" + countryCode2 + "\"";
                                ubb = executeSelect(commandCurrentCity);
                                //Console.WriteLine(commandCurrentCity.CommandText);

                                if (ubb.Tables[0].Rows.Count != 0)
                                {
                                    bool insertUser = true;
                                    bool active = true;
                                    if (!(gen.NextDouble() >= 0.5))
                                    {
                                        active = false;
                                    }
                                    bool client = false;
                                    if (i % 7 == 0)
                                    {
                                        client = true;

                                        if (!(gen.NextDouble() >= 0.7))
                                        {
                                            insertUser = false;
                                        }
                                    }

                                    Console.WriteLine();
                                    Console.WriteLine("---------------------------------" + (i + 1) + ". OSOBA---------------------------------");
                                    Console.WriteLine("user_id: 0");
                                    Console.WriteLine("username: " + values["results"][0]["login"]["username"]);
                                    Console.WriteLine("password: " + values["results"][0]["login"]["password"]);
                                    Console.WriteLine("email: " + values["results"][0]["email"]);
                                    Console.WriteLine("first_name: " + firstName);
                                    Console.WriteLine("last_name: " + lastName);
                                    Console.WriteLine("birthday: " + birthdayData[2] + "-" + birthdayData[0] + "-" + birthdayData[1]);
                                    Console.WriteLine("gender_id: " + gender);
                                    Console.WriteLine("date_created: " + dateCreatedData[2] + "-" + dateCreatedData[0] + "-" + dateCreatedData[1]);
                                    Console.WriteLine("date_updated: " + date_updated.ToString("yyyy-MM-dd"));
                                    Console.WriteLine("Home city: " + city);
                                    Console.WriteLine("hometown_id: " + homeCityId);
                                    Console.WriteLine("country code: " + countryCode);
                                    Console.WriteLine("current city: " + current_city);
                                    Console.WriteLine("current_town_id: " + ubb.Tables[0].Rows[0][0]);
                                    Console.WriteLine("country code: " + countryCode2);

                                    if (insertUser) { 
                                        string queryUser = "INSERT INTO users VALUES (0, " + "\"" + values["results"][0]["login"]["username"] + "\"" + ", " +
                                                 "\"" + values["results"][0]["login"]["password"] + "\"" + ", " +
                                                 "\"" + values["results"][0]["email"] + "\"" + ", " +
                                                 "\"" + firstName + "\"" + ", " +
                                                 "\"" + lastName + "\"" + ", " +
                                                 "\"" + birthdayData[2] + "-" + birthdayData[0] + "-" + birthdayData[1] + "\"" + ", " +
                                                 gender + ", " +
                                                 "\"" + dateCreatedData[2] + "-" + dateCreatedData[0] + "-" + dateCreatedData[1] + "\"" + ", " +
                                                 "\"" + date_updated.ToString("yyyy-MM-dd") + "\"" + ", " +
                                                active + ", " +
                                                homeCityId + ", " +
                                                ubb.Tables[0].Rows[0][0] + ")";

                                        command.CommandText = queryUser;
                                        executeNonQuery(command);
                                    }

                                    if (!active && insertUser)
                                    {
                                        DateTime date_deleted = new DateTime(date_updated.Year, date_updated.Month, date_updated.Day + 1);
                                        range = (DateTime.Today - date_deleted).Days;
                                        date_deleted = date_deleted.AddDays(gen.Next(range));
                                        Console.WriteLine("date_deleted: " + date_deleted.ToString("yyyy-MM-dd"));

                                        int idReason = gen.Next(6);
                                        string reason = "Other";
                                        if (idReason == 1)
                                        {
                                            reason = "I don't like this anymore";
                                        }
                                        else if (idReason == 2)
                                        {
                                            reason = "Found something better";
                                        }
                                        else if (idReason == 3)
                                        {
                                            reason = "Too much technical problems";
                                        }
                                        else if (idReason == 4)
                                        {
                                            reason = "Personal reasons";
                                        }
                                        else if (idReason == 5)
                                        {
                                            reason = "Deleted by admin";
                                        }
                                        Console.WriteLine("reason: " + reason);


                                        MySqlCommand commandGetUserId = conn.CreateCommand();
                                        commandGetUserId.CommandText = "SELECT users.user_id FROM users WHERE users.username = '" + values["results"][0]["login"]["username"] + "'";
                                        System.Data.DataSet ubb2 = executeSelect(commandGetUserId);

                                        if (queryDeletedUsers == "INSERT INTO deleted_users VALUES")
                                        {
                                            queryDeletedUsers += "(" + ubb2.Tables[0].Rows[0][0] + ", " + "\"" + date_deleted.ToString("yyyy-MM-dd") + "\"" + ", " + "\"" + reason + "\"" + ")";
                                        }
                                        else
                                        {
                                            queryDeletedUsers += ", (" + ubb2.Tables[0].Rows[0][0] + ", " + "\"" + date_deleted.ToString("yyyy-MM-dd") + "\"" + ", " + "\"" + reason + "\"" + ")";
                                        }

                                        command.CommandText = "INSERT INTO deleted_users VALUES (" + ubb2.Tables[0].Rows[0][0] + ", " + "\"" + date_deleted.ToString("yyyy-MM-dd") + "\"" + ", " + "\"" + reason + "\"" + ")";
                                        executeNonQuery(command);
                                        //Console.WriteLine(queryDeletedUser);
                                    }

                                    if (client)
                                    {
                                        Console.WriteLine("Client");

                                        int credit_card;
                                        while (true)
                                        {
                                            credit_card = gen.Next(10000000, 99999999);
                                            command.CommandText = "SELECT * FROM clients WHERE clients.credit_card_number = " + credit_card;
                                            System.Data.DataSet ubb3 = executeSelect(command);
                                            if (ubb3.Tables[0].Rows.Count == 0) {
                                                break;
                                            }
                                        }

                                        Console.WriteLine("credit card: " + credit_card);
                                        Console.WriteLine("billing address: " + values2["results"][0]["location"]["street"]);
                                        Console.WriteLine("CURRENT TOWN ID: " + ubb.Tables[0].Rows[0][0]);
                                        if (!insertUser)
                                        {
                                            Console.WriteLine("*****OVAJ KLIJENT NE POSTOJI KAO USER******");
                                        }

                                        DateTime date_client_created = new DateTime(Int32.Parse(dateCreatedData[2]) + 1, 1, 1);
                                        range = (DateTime.Today - date_client_created).Days;
                                        date_client_created = date_client_created.AddDays(gen.Next(range));
                                        Console.WriteLine("date_client_created: " + date_client_created.ToString("yyyy-MM-dd"));

                                        if (queryClients == "INSERT INTO clients VALUES")
                                        {
                                            queryClients += "(0, \"" + firstName + "\", \"" + lastName + "\", \"" + values2["results"][0]["location"]["street"] + "\", " + ubb.Tables[0].Rows[0][0] +
                                                            ", " + credit_card + ", \"" + date_client_created.ToString("yyyy-MM-dd") + "\")";
                                        }
                                        else
                                        {
                                            queryClients += ", (0, \"" + firstName + "\", \"" + lastName + "\", \"" + values2["results"][0]["location"]["street"] + "\", " + ubb.Tables[0].Rows[0][0] +
                                                            ", " + credit_card + ", \"" + date_client_created.ToString("yyyy-MM-dd") + "\")";
                                        }

                                        command.CommandText = "INSERT INTO clients VALUES (0, \"" + firstName + "\", \"" + lastName + "\", \"" + values2["results"][0]["location"]["street"] + "\", " + ubb.Tables[0].Rows[0][0] +
                                                            ", " + credit_card + ", \"" + date_client_created.ToString("yyyy-MM-dd") + "\")";
                                        executeNonQuery(command);
                                    }

                                    if (insertUser) {
                                        if (queryUsers == "INSERT INTO users VALUES")
                                        {
                                            queryUsers += "(0, " + "\"" + values["results"][0]["login"]["username"] + "\"" + ", " +
                                                 "\"" + values["results"][0]["login"]["password"] + "\"" + ", " +
                                                 "\"" + values["results"][0]["email"] + "\"" + ", " +
                                                 "\"" + firstName + "\"" + ", " +
                                                 "\"" + lastName + "\"" + ", " +
                                                 "\"" + birthdayData[2] + "-" + birthdayData[0] + "-" + birthdayData[1] + "\"" + ", " +
                                                 gender + ", " +
                                                 "\"" + dateCreatedData[2] + "-" + dateCreatedData[0] + "-" + dateCreatedData[1] + "\"" + ", " +
                                                 "\"" + date_updated.ToString("yyyy-MM-dd") + "\"" + ", " +
                                                active + ", " +
                                                homeCityId + ", " +
                                                ubb.Tables[0].Rows[0][0] + ")";
                                            break;
                                        }
                                        queryUsers += ", (0, " + "\"" + values["results"][0]["login"]["username"] + "\"" + ", " +
                                                 "\"" + values["results"][0]["login"]["password"] + "\"" + ", " +
                                                 "\"" + values["results"][0]["email"] + "\"" + ", " +
                                                 "\"" + firstName + "\"" + ", " +
                                                 "\"" + lastName + "\"" + ", " +
                                                 "\"" + birthdayData[2] + "-" + birthdayData[0] + "-" + birthdayData[1] + "\"" + ", " +
                                                 gender + ", " +
                                                 "\"" + dateCreatedData[2] + "-" + dateCreatedData[0] + "-" + dateCreatedData[1] + "\"" + ", " +
                                                 "\"" + date_updated.ToString("yyyy-MM-dd") + "\"" + ", " +
                                                active + ", " +
                                                homeCityId + ", " +
                                                ubb.Tables[0].Rows[0][0] + ")";
                                    }

                                    break;
                                }
                            }


                            reader.Close();
                            dataStream.Close();
                            response.Close();
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }

            string cd = Directory.GetCurrentDirectory();

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(cd, "users.txt")))
            {
                outputFile.WriteLine(queryUsers);
            };

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(cd, "deleted_users.txt")))
            {
                outputFile.WriteLine(queryDeletedUsers);
            };

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(cd, "clients.txt")))
            {
                outputFile.WriteLine(queryClients);
            };

            conn.Close();
            Console.WriteLine();
            /*Console.WriteLine(queryUsers);
            Console.WriteLine();
            Console.WriteLine(queryDeletedUsers);*/
        }

        public static void updateAndFillCountriesStatesCitiesTables(MySqlCommand command)
        {
            string cd = Directory.GetCurrentDirectory();
            // 500 API ZAHTEVA PO DANU PO KLJUCU
            string[] keys = { "bc8c52cc9a0237aa093fe627bf78b89e", "6b4a8953674984a2f4a8203e89067deb", "0c96e6fb8ca035264d26fa2e003c54da", "76e3a0a3c8ad55e6946880920c1a4039", "075b5bb4708351f5bcfa18c3dbfb61ea",
            "60ad081e577c2fc571a06fb275e6c642", "2808ee51eadca5dcb83eaa0c59016f58", "0a2be5e149d17a1a7500f9e3759cc7c7", "ebc884cb84359ade3e2862d1155a286b", "a54c4d3efda60284d247a12a8d9f7c38", "dfd6433015275967e37f62e45a9b3ffa",
            "cec6d91a600a09e15cf4c32263d5984c", "cc8109c648a1ad437fc560c11b7915c2"};
            string key = "bc8c52cc9a0237aa093fe627bf78b89e";
            int numbersOfRequestedApi = 0;


            List<Dictionary<string, string>> listOfCountries = getListOfCountries(key);
            numbersOfRequestedApi++;
            Console.WriteLine("radim...");
            key = changeKey(key, keys);

            int counterForCountries = 0;
            string queryForCountries = "INSERT INTO countries VALUES";
            string queryForStates = "INSERT INTO states VALUES";
            string queryForCities = "INSERT INTO cities VALUES";

            foreach (Dictionary<string, string> dict in listOfCountries)
            {
                //REGIONS--------------------------------------------------------------------------------------------------
                try
                {
                    List<Dictionary<string, string>> listOfRegions = getListOfRegions(dict["code"], key);
                    numbersOfRequestedApi++;
                    //Console.WriteLine("Broj poslatih zahteva za api: " + numbersOfRequestedApi);
                    key = changeKey(key, keys);

                    if (listOfRegions.Count != 0)
                    {

                        if (queryForCountries == "INSERT INTO countries VALUES")
                        {
                            queryForCountries += "(\"" + dict["code"] + "\", \"" + dict["name"] + "\")";
                        }
                        else
                        {
                            queryForCountries += ", (\"" + dict["code"] + "\", \"" + dict["name"] + "\")";
                        }

                        //Console.WriteLine("Zapisujem DRZAVU");
                        command.CommandText = "INSERT INTO countries VALUES" + "(\"" + dict["code"] + "\", \"" + dict["name"] + "\")"; ;
                        executeNonQuery(command);
                        counterForCountries++;


                        int counterForStates = 0;
                        foreach (Dictionary<string, string> dictOfRegions in listOfRegions)
                        {
                            try
                            {
                                List<Dictionary<string, string>> listOfCities = getListOfCities(dict["code"], dictOfRegions["region"], key);
                                numbersOfRequestedApi++;
                                //Console.WriteLine("Broj poslatih zahteva za api: " + numbersOfRequestedApi);
                                key = changeKey(key, keys);

                                //INSERT INTO cities VALUES (0, "name", 8, "code");

                                if (listOfCities.Count != 0)
                                {
                                    if (queryForStates == "INSERT INTO states VALUES")
                                    {
                                        queryForStates += "(0, " + "\"" + dictOfRegions["region"] + "\", \"" + dictOfRegions["country"] + "\")";
                                        counterForStates++;
                                    }
                                    else
                                    {
                                        queryForStates += ", (0, " + "\"" + dictOfRegions["region"] + "\", \"" + dictOfRegions["country"] + "\")";
                                        counterForStates++;
                                    }

                                    //Console.WriteLine("Zapisujem REGION");
                                    command.CommandText = "INSERT INTO states VALUES" + "(0, " + "\"" + dictOfRegions["region"] + "\", \"" + dictOfRegions["country"] + "\")";
                                    executeNonQuery(command);
                                    counterForStates++;


                                    //Console.WriteLine(queryForStates);

                                    foreach (Dictionary<string, string> dictOfCities in listOfCities)
                                    {
                                        if (dictOfCities["city"] == "Gemeente \u0027s-Hertogenbosch")
                                        {
                                            continue;
                                        }

                                        command.CommandText = "SELECT states.state_id FROM states WHERE states.name = " + "\"" + dictOfRegions["region"] + "\" AND states.country_code = " + "\"" + dictOfCities["country"] + "\"";
                                        System.Data.DataSet ubb = executeSelect(command);


                                        if (queryForCities == "INSERT INTO cities VALUES")
                                        {
                                            queryForCities += "(0, " + "\"" + dictOfCities["city"] + "\", " + ubb.Tables[0].Rows[0][0] + ")";
                                        }
                                        else
                                        {
                                            queryForCities += ", (0, " + "\"" + dictOfCities["city"] + "\", " + ubb.Tables[0].Rows[0][0] + ")";
                                        }

                                        //Console.WriteLine("Zapisujem GRAD");
                                        command.CommandText = "INSERT INTO cities VALUES" + "(0, " + "\"" + dictOfCities["city"] + "\", " + ubb.Tables[0].Rows[0][0] + ")";
                                        executeNonQuery(command);
                                    }
                                }

                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                //--------------------------------------------------------------------------------------------------
            }
            queryForCities += ";";
            queryForStates += ";";
            queryForCountries += ";";

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(cd, "cities.txt")))
            {
                outputFile.WriteLine(queryForCities);
            };
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(cd, "states.txt")))
            {
                outputFile.WriteLine(queryForStates);
            };
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(cd, "countries.txt")))
            {
                outputFile.WriteLine(queryForCountries);
            };

            Console.WriteLine("Gotovo pritisni dirku");
            Console.ReadKey();
        }

        public static void updateAndFillLogTime(MySqlCommand command) {
            string query = "INSERT INTO log_time VALUES";
            command.CommandText = "SELECT * FROM users";
            Random gen = new Random();

            System.Data.DataSet ubb = executeSelect(command);

            for (int i = 0; i < ubb.Tables[0].Rows.Count; i++) {
                //Console.WriteLine(ubb.Tables[0].Rows[i][0] + " " + ubb.Tables[0].Rows[i]["date_created"] + " " + ubb.Tables[0].Rows[i]["date_updated"]);

                string date_created_string = ubb.Tables[0].Rows[i]["date_created"].ToString();
                date_created_string = date_created_string.Split(' ')[0];
                DateTime date_created = new DateTime(Int32.Parse(date_created_string.Split('/')[2]), Int32.Parse(date_created_string.Split('/')[0]), Int32.Parse(date_created_string.Split('/')[1]));

                string date_updated_string = ubb.Tables[0].Rows[i]["date_updated"].ToString();
                date_updated_string = date_updated_string.Split(' ')[0];
                DateTime date_updated = new DateTime(Int32.Parse(date_updated_string.Split('/')[2]), Int32.Parse(date_updated_string.Split('/')[0]), Int32.Parse(date_updated_string.Split('/')[1]));

                
                int days_between = (date_updated - date_created).Days;
                //Console.WriteLine("Br DANA: " + days_between);
                List<List<DateTime>> logovanja = new List<List<DateTime>>();
                for (int j = 0; j < days_between; j++) {

                    int broj_logovanja = gen.Next(1, 4);
                    List<DateTime> logovanja_u_toku_dana = new List<DateTime>();
                    DateTime temp = new DateTime();

                    int uvecanje = 24 / broj_logovanja;
                    int sati_za_preskok = 0;
                    int var = 0;

                    //Console.WriteLine("Br logovanja u toku dana " + j + ". : " + broj_logovanja);
                    date_created = date_created.AddDays(1);
                    for (int k = 0; k < broj_logovanja; k++) {
                        DateTime login;
                        DateTime logout;
                        sati_za_preskok += uvecanje;



                        while (true)
                        {
                            login = new DateTime(date_created.Year, date_created.Month, date_created.Day, gen.Next(var, sati_za_preskok), gen.Next(0, 60), gen.Next(0, 60));
                            if (k > 0)
                            {
                                if (login < temp) {
                                    continue;
                                }
                            }


                            logout = new DateTime(date_created.Year, date_created.Month, date_created.Day, gen.Next(var, sati_za_preskok), gen.Next(0, 60), gen.Next(0, 60));
                            if (logout > login)
                            {
                                break;
                            }
                        }
                        temp = logout;
                        logovanja_u_toku_dana.Add(login);
                        logovanja_u_toku_dana.Add(logout);
                        var += uvecanje;                        
                    }
                    logovanja.Add(logovanja_u_toku_dana);
                }

                foreach (List<DateTime> u_danu in logovanja)
                {
                    //Console.WriteLine("------------- U TOKU DANA ------------------------");
                    for (int j = 0; j < u_danu.Count; j = j + 2) {
                        if (j % 2 == 0)
                        {
                            //Console.WriteLine("LOGIN: " + u_danu[j].ToString());
                            
                        }
                        else {
                            //Console.WriteLine("LOGOUT: " + u_danu[j].ToString());

                        }

                        //Console.WriteLine("INSERT INTO log_time VALUES(0, " + ubb.Tables[0].Rows[i][0] + ", \"" + u_danu[j].ToString() + "\", \"" + u_danu[j + 1].ToString() + "\")");
                        command.CommandText = "INSERT INTO log_time VALUES(0, " + ubb.Tables[0].Rows[i][0] + ", STR_TO_DATE(\"" + u_danu[j].ToString() + "\", '%m/%d/%Y %h:%i:%s %p'), STR_TO_DATE(\"" + u_danu[j + 1].ToString() + "\", '%m/%d/%Y %h:%i:%s %p'))";
                        executeNonQuery(command);
                        if (query == "INSERT INTO log_time VALUES")
                        {
                            query += "(0, " + ubb.Tables[0].Rows[i][0] + ", STR_TO_DATE(\"" + u_danu[j].ToString() + "\", '%m/%d/%Y %h:%i:%s %p'), STR_TO_DATE(\"" + u_danu[j + 1].ToString() + "\", '%m/%d/%Y %h:%i:%s %p'))";
                        }
                        else
                        {
                            query += ", (0, " + ubb.Tables[0].Rows[i][0] + ", STR_TO_DATE(\"" + u_danu[j].ToString() + "\", '%m/%d/%Y %h:%i:%s %p'), STR_TO_DATE(\"" + u_danu[j + 1].ToString() + "\", '%m/%d/%Y %h:%i:%s %p'))";
                        }
                    }
                    //Console.WriteLine("------------------------------------------------------");
                }
            }
            //Console.WriteLine(query);

            string cd = Directory.GetCurrentDirectory();

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(cd, "log_time.txt")))
            {
                outputFile.WriteLine(query);
            };
        }

        private static void fillTables(string connetionString, MySqlCommand command)
        {
            command.CommandText = System.IO.File.ReadAllText(@"../Release/countries.txt");
            executeNonQuery(command);
            command.CommandText = System.IO.File.ReadAllText(@"../Release/states.txt");
            executeNonQuery(command);
            command.CommandText = System.IO.File.ReadAllText(@"../Release/cities.txt");
            executeNonQuery(command);

            command.CommandText = System.IO.File.ReadAllText(@"../Release/users.txt");
            executeNonQuery(command);
            command.CommandText = System.IO.File.ReadAllText(@"../Release/clients.txt");
            executeNonQuery(command);
            command.CommandText = System.IO.File.ReadAllText(@"../Release/deleted_users.txt");
            executeNonQuery(command);

            command.CommandText = System.IO.File.ReadAllText(@"../Release/multimedia.txt");
            executeNonQuery(command);
            command.CommandText = System.IO.File.ReadAllText(@"../Release/friend_requests.txt");
            executeNonQuery(command);
            command.CommandText = System.IO.File.ReadAllText(@"../Release/friends.txt");
            executeNonQuery(command);
            command.CommandText = System.IO.File.ReadAllText(@"../Release/log_time.txt");
            executeNonQuery(command);
            command.CommandText = System.IO.File.ReadAllText("..\\..\\..\\podaci\\grupe.txt");
            executeNonQuery(command);
            command.CommandText = System.IO.File.ReadAllText("..\\..\\..\\podaci\\postLikesCommentsMessages.txt");
            executeNonQuery(command);
            command.CommandText = System.IO.File.ReadAllText("..\\..\\..\\podaci\\advertisements.txt");
            executeNonQuery(command);
        }

        public static void updateAndFillFriendRequestsFriends(MySqlCommand command) {

            Random gen = new Random();
            string queryFriends = "INSERT INTO friends VALUES";
            string queryFriendRequests = "INSERT INTO friend_requests VALUES";
            command.CommandText = "SELECT COUNT(*) FROM users";
            System.Data.DataSet ubb = executeSelect(command);
            int sum = Int32.Parse(ubb.Tables[0].Rows[0][0].ToString());

            int requestFriendId;
            int friendId;
            string statusString;
            for (int j = 0; j < 10000; j++) {
                statusString = "sent";
                while (true) {

                    requestFriendId = gen.Next(1, sum);

                    while (true)
                    {
                        friendId = gen.Next(1, sum);
                        if (friendId != requestFriendId)
                        {
                            break;
                        }
                    }

                    int status = gen.Next(1, 4);
                    if (status == 1)
                    {
                        statusString = "rejected";
                    }
                    else if (status == 2)
                    {
                        statusString = "friends";
                    }


                    if (statusString == "friends")
                    {
                        command.CommandText = "SELECT * FROM friend_requests WHERE (friend_requests.user_id_from = " + requestFriendId + " AND friend_requests.user_id_to = " + friendId + ") OR (friend_requests.user_id_from = " + friendId + " AND friend_requests.user_id_to = " + requestFriendId + ") AND friend_requests.status = 'friends'";
                        System.Data.DataSet ubb2 = executeSelect(command);
                        if (ubb2.Tables[0].Rows.Count == 0)
                        {

                            command.CommandText = "SELECT * FROM friend_requests WHERE (friend_requests.user_id_from = " + friendId + " AND friend_requests.user_id_to = " + requestFriendId + ") AND friend_requests.status = 'rejected'";
                            ubb2 = executeSelect(command);
                            if (ubb2.Tables[0].Rows.Count == 0)
                            {
                                command.CommandText = "SELECT * FROM friend_requests WHERE (friend_requests.user_id_from = " + friendId + " AND friend_requests.user_id_to = " + requestFriendId + ") AND friend_requests.status = 'sent'";
                                ubb2 = executeSelect(command);
                                if (ubb2.Tables[0].Rows.Count != 0)
                                {
                                    break;
                                }
                                else
                                {
                                    statusString = "sent";
                                    break;
                                }
                            }
                        }
                    } else if (statusString == "rejected") {

                        command.CommandText = "SELECT * FROM friend_requests WHERE (friend_requests.user_id_from = " + requestFriendId + " AND friend_requests.user_id_to = " + friendId + ") OR (friend_requests.user_id_from = " + friendId + " AND friend_requests.user_id_to = " + requestFriendId + ") AND friend_requests.status = 'friends'";
                        System.Data.DataSet ubb2 = executeSelect(command);
                        if (ubb2.Tables[0].Rows.Count == 0)
                        {
                            command.CommandText = "SELECT * FROM friend_requests WHERE (friend_requests.user_id_from = " + friendId + " AND friend_requests.user_id_to = " + requestFriendId + ") AND friend_requests.status = 'sent'";
                            ubb2 = executeSelect(command);
                            if (ubb2.Tables[0].Rows.Count != 0)
                            {
                                break;
                            }
                        }
                    }
                    else {

                        command.CommandText = "SELECT * FROM friend_requests WHERE (friend_requests.user_id_from = " + requestFriendId + " AND friend_requests.user_id_to = " + friendId + ") OR (friend_requests.user_id_from = " + friendId + " AND friend_requests.user_id_to = " + requestFriendId + ") AND friend_requests.status = 'friends'";
                        System.Data.DataSet ubb2 = executeSelect(command);
                        if (ubb2.Tables[0].Rows.Count == 0)
                        {
                            command.CommandText = "SELECT * FROM friend_requests WHERE (friend_requests.user_id_from = " + friendId + " AND friend_requests.user_id_to = " + requestFriendId + ") AND friend_requests.status = 'rejected'";
                            ubb2 = executeSelect(command);
                            if (ubb2.Tables[0].Rows.Count == 0)
                            {
                                break;
                            }
                        }

                    }
                }

                DateTime data_created;
                DateTime data_updated;

                if (statusString == "sent")
                {
                    command.CommandText = "SELECT users.date_created, users.date_updated FROM users WHERE users.user_id = " + requestFriendId;
                    ubb = executeSelect(command);

                    string date_created_string = ubb.Tables[0].Rows[0][0].ToString();
                    date_created_string = date_created_string.Split(' ')[0];
                    data_created = new DateTime(Int32.Parse(date_created_string.Split('/')[2]), Int32.Parse(date_created_string.Split('/')[0]), Int32.Parse(date_created_string.Split('/')[1]));

                    string data_updated_string = ubb.Tables[0].Rows[0][1].ToString();
                    data_updated_string = data_updated_string.Split(' ')[0];
                    data_updated = new DateTime(Int32.Parse(data_updated_string.Split('/')[2]), Int32.Parse(data_updated_string.Split('/')[0]), Int32.Parse(data_updated_string.Split('/')[1]));

                }
                else {
                    command.CommandText = "SELECT friend_requests.date_created FROM friend_requests WHERE friend_requests.user_id_to = " + requestFriendId + " AND friend_requests.user_id_from = " + friendId + " AND friend_requests.status = 'sent'";
                    ubb = executeSelect(command);

                    string date_created_string = ubb.Tables[0].Rows[0][0].ToString();
                    date_created_string = date_created_string.Split(' ')[0];
                    data_created = new DateTime(Int32.Parse(date_created_string.Split('/')[2]), Int32.Parse(date_created_string.Split('/')[0]), Int32.Parse(date_created_string.Split('/')[1]));

                    command.CommandText = "SELECT users.date_updated FROM users WHERE users.user_id = " + requestFriendId;
                    ubb = executeSelect(command);

                    string data_updated_string = ubb.Tables[0].Rows[0][0].ToString();
                    data_updated_string = data_updated_string.Split(' ')[0];
                    data_updated = new DateTime(Int32.Parse(data_updated_string.Split('/')[2]), Int32.Parse(data_updated_string.Split('/')[0]), Int32.Parse(data_updated_string.Split('/')[1]));

                    if (data_created > data_updated) {
                        j--;
                        continue;
                    }
                }


                int range = (data_updated - data_created).Days;
                DateTime date = data_created.AddDays(gen.Next(range));



                if (statusString == "friends")
                {
                    command.CommandText = "INSERT INTO friends VALUES (" + requestFriendId + ", " + friendId + ")";
                    executeNonQuery(command);
                    command.CommandText = "INSERT INTO friends VALUES (" + friendId + ", " + requestFriendId + ")";
                    executeNonQuery(command);

                    int temp;
                    for (int i = 0; i < 2; i++)
                    {
                        if (queryFriends == "INSERT INTO friends VALUES")
                        {
                            queryFriends += "(" + requestFriendId + ", " + friendId + ")";
                        }
                        else
                        {
                            queryFriends += ", (" + requestFriendId + ", " + friendId + ")";
                        }
                        temp = requestFriendId;
                        requestFriendId = friendId;
                        friendId = temp;
                    }
                }

                

                if (queryFriendRequests == "INSERT INTO friend_requests VALUES")
                {
                    queryFriendRequests += "(" + requestFriendId + ", " + friendId + ", \"" + statusString + "\", \"" + date.Year + "-" + date.Month + "-" + date.Day + "\")";
                }
                else
                {
                    queryFriendRequests += ", (" + requestFriendId + ", " + friendId + ", \"" + statusString + "\", \"" + date.Year + "-" + date.Month + "-" + date.Day + "\")";
                }

                command.CommandText = "INSERT INTO friend_requests VALUES (" + requestFriendId + ", " + friendId + ", \"" + statusString + "\", \"" + date.Year + "-" + date.Month + "-" + date.Day + "\")";
                executeNonQuery(command);
                

                /*Console.WriteLine(data_created);
                Console.WriteLine(data_updated);
                Console.WriteLine(date);
                Console.WriteLine(requestFriendId);
                Console.WriteLine(friendId);
                Console.WriteLine(statusString);*/
            }


            string cd = Directory.GetCurrentDirectory();

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(cd, "friends.txt")))
            {
                outputFile.WriteLine(queryFriends);
            };

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(cd, "friend_requests.txt")))
            {
                outputFile.WriteLine(queryFriendRequests);
            };

        }

        public static int userCount(MySqlCommand command)
        {
            command.CommandText = "SELECT COUNT(*) FROM users";
            System.Data.DataSet ds = executeSelect(command);
            int user_count = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            //Console.WriteLine(user_count);
            return user_count;
        }

        public static int groupCount(MySqlCommand command)
        {
            command.CommandText = "SELECT COUNT(*) FROM groups";
            System.Data.DataSet ds = executeSelect(command);
            int group_count = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            //Console.WriteLine(group_count);
            return group_count;
        }

        public static int multimediaCount(MySqlCommand command)
        {
            command.CommandText = "SELECT COUNT(*) FROM multimedia";
            System.Data.DataSet ds = executeSelect(command);
            int multimedia_count = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            //Console.WriteLine(multimedia_count);
            return multimedia_count;
        }

        public static int clientCount(MySqlCommand command)
        {
            command.CommandText = "SELECT COUNT(*) FROM clients";
            System.Data.DataSet ds = executeSelect(command);
            int client_count = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            //Console.WriteLine(client_count);
            return client_count;
        }

        public static int postCount(MySqlCommand command)
        {
            command.CommandText = "SELECT COUNT(*) FROM posts_likes_comments_messages";
            System.Data.DataSet ds = executeSelect(command);
            int post_count = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            //Console.WriteLine(post_count);
            return post_count;
        }

        public static void fillGroupTable(MySqlCommand command)
        {
            List<string> imena_grupa = new List<string>();
            List<string> kategorije = new List<string>(new string[]{"vacation", "animals", "babies", "food", "love", "dogs", "cats", "wild animals",
                "clothing", "fashion", "fans", "music", "TV shows", "movies", "coffee", "cartoon", "superheroes", "politics", "games", "sports" });
            string[] lines = File.ReadAllLines("..\\..\\..\\podaci\\groups.txt");

            foreach (string line in lines)
                imena_grupa.Add(line);

            int users = userCount(command);

            //Console.WriteLine(imena_grupa.Count);

            

            Random dgen = new Random();
            Random ygen = new Random();
            Random agen = new Random();
            int brojac = 1;
            int id = 1;

            string unos = "INSERT INTO groups VALUES ";
            //MySqlCommand queryGroups = conn.CreateCommand();
            for (int i = 0; i < imena_grupa.Count; i++)
            {
                DateTime privremeni = DateTime.Now;
                string category = "";
                int cbool = agen.Next(0, kategorije.Count);
                category = kategorije[cbool];

                string type = "";
                int tenum = agen.Next(0, 10);
                if (tenum == 1)
                {
                    type = "private";
                }
                else
                {
                    type = "public";
                }

                string info = "";

                List<string> komentari = new List<string>();
                string[] infoKomentar = File.ReadAllLines("..\\..\\..\\podaci\\komentari.txt");

                foreach (string line in infoKomentar)
                {
                    komentari.Add(line);
                }
                int irand = agen.Next(0, komentari.Count);
                info = komentari[irand];

                for (int j = 0; j < 10; j++)
                {
                                     
                    string querryGroup = "( ";
                    querryGroup += id + ", ";
                    querryGroup += "'" + imena_grupa[i] + "'"  + ", ";

                    
                    bool datum_provera = false;
                    string date = "";
                    int fbool = 0;
                    while (datum_provera == false)
                    {
                        fbool = agen.Next(1, users + 1);
                        int num = ygen.Next(0, 20);
                        int range = num * 365;
                        DateTime randomDate = DateTime.Today.AddDays(-dgen.Next(range));
                        
                        

                        command.CommandText = "SELECT date_created FROM users WHERE user_id = " + fbool;
                        System.Data.DataSet ds = executeSelect(command);
                        var user_date = ds.Tables[0].Rows[0][0].ToString();
                        DateTime datum = Convert.ToDateTime(user_date);

                        command.CommandText = "SELECT date_updated FROM users WHERE user_id = " + fbool;
                        ds = executeSelect(command);
                        var user_date_updated = ds.Tables[0].Rows[0][0].ToString();
                        DateTime datum_updated = Convert.ToDateTime(user_date_updated);

                        //Console.WriteLine(randomDate + "\n" + datum + "\n" + privremeni);
                        if (brojac == 1 && randomDate > datum && randomDate < datum_updated)
                        {
                            privremeni = randomDate;
                            datum_provera = true;
                            //Console.WriteLine("proslo ok");

                        }
                        else if (randomDate > privremeni && randomDate < datum_updated)
                        {
                            datum_provera = true;
                        }
                        
                        date = onlyDate(randomDate);
                    }

                    



                    //Console.WriteLine(date);
                    querryGroup += "'" + date + "'" + ", ";


                    bool active = true;

                    int abool = agen.Next(0, 5);
                    //Console.WriteLine(abool);
                    if (abool == 0)
                    {
                        active = false;
                    }
                    

                    querryGroup += active + ", ";

                                        
                    querryGroup += "'" + type + "'" + ", ";

                    
                    querryGroup += "'" + info + "'" + ", ";

                    
                    querryGroup += "'" + category + "'" + ", ";

                    
                    querryGroup += fbool + ", ";

                    
                    string mtype = "";
                    if (brojac == 1)
                    {
                        mtype = "creator";
                        brojac++;
                    }
                    else
                    {
                        int mbool = agen.Next(0, 10);
                        if (mbool == 1)
                        {
                            mtype = "admin";
                        }
                        else
                        {
                            mtype = "member";
                        }
                        brojac++;
                    }


                    querryGroup += "'" + mtype + "'" + "),";

                    //command.CommandText = querryGroup;
                    //Console.ReadKey();
                    //Console.WriteLine(querryGroup);
                    //Console.ReadKey();
                    //executeNonQuery(command);
                    unos += querryGroup;
                }
                id++;
                brojac = 1;
                

            }
            //unos.TrimEnd(';');
            string unosUFajl =  unos.Substring(0, unos.Length - 1);
            Console.WriteLine(unosUFajl);
            System.IO.File.WriteAllText("..\\..\\..\\podaci\\grupe.txt", unosUFajl);

        }

        private static void fillPostLikesCommentsMessages(MySqlCommand command)
        {
            int user_count = userCount(command);
            int group_count = groupCount(command);
            int multimedia_count = multimediaCount(command);


            List<string> komentari = new List<string>();
            string[] lines = File.ReadAllLines("..\\..\\..\\podaci\\komentari.txt");

            foreach (string line in lines)
            {
                komentari.Add(line);
                //Console.WriteLine(line);
            }
            //Console.WriteLine(komentari.Count());


            Random gen = new Random();
            int post_number = 0;
            int id = 1;

            string unosPosts = "INSERT INTO posts_likes_comments_messages VALUES ";

            for (int i = 0; i < 100; i++)
            {
                DateTime privremeni = DateTime.Now;

                int random = gen.Next(1, 15);

                
                //Console.WriteLine(random);
                for (int j = 0; j < random; j++)
                {
                    int post_id = id;

                    int user_id_from = 0;
                    bool datum_provera = false;
                    string date = "";
                    DateTime randomDate = new DateTime();
                    while (datum_provera == false)
                    {
                        user_id_from = gen.Next(1, user_count + 1);
                        int num = gen.Next(0, 20);
                        int range = num * 365;
                        randomDate = DateTime.Today.AddDays(-gen.Next(range));



                        command.CommandText = "SELECT date_created FROM users WHERE user_id = " + user_id_from;
                        System.Data.DataSet ds = executeSelect(command);
                        var user_date = ds.Tables[0].Rows[0][0].ToString();
                        DateTime datum = Convert.ToDateTime(user_date);

                        command.CommandText = "SELECT date_updated FROM users WHERE user_id = " + user_id_from;
                        ds = executeSelect(command);
                        var user_date_updated = ds.Tables[0].Rows[0][0].ToString();
                        DateTime datum_updated = Convert.ToDateTime(user_date_updated);

                        //Console.WriteLine(randomDate + "\n" + datum + "\n" + privremeni);
                        if (post_number == 0)
                        {
                            if (randomDate > datum && randomDate < datum_updated)
                            {
                                datum_provera = true;
                            }
                        }
                        else {
                            if (randomDate > privremeni && randomDate < datum_updated && randomDate > datum) {
                                datum_provera = true;
                            }
                        }


                        date = onlyDate(randomDate);


                    }

                    bool active = true;

                    int abool = gen.Next(0, 5);
                    //Console.WriteLine(abool);
                    if (abool == 0)
                    {
                        active = false;
                    }

                    string privacy = "";

                    int pran = gen.Next(0, 5);
                    //Console.WriteLine(abool);
                    if (pran == 0)
                    {
                        privacy = "private";
                    } else
                    {
                        privacy = "public";
                    }

                    int multimedia_id = 0;
                    int group_id = 0;
                    int user_id_to = 0;
                    string caption_content = "";
                    string action_type = "";

                    if (post_number == 0)
                    {
                        action_type = "poster";
                        post_number++;
                        privremeni = randomDate;


                        int gran = gen.Next(0, 2);
                        if (gran == 1)
                        {
                            int gid = gen.Next(1, group_count + 1);
                            group_id = gid;
                        }

                        int mran = gen.Next(0, 2);
                        if (mran == 1)
                        {
                            int mid = gen.Next(1, multimedia_count + 1);
                            multimedia_id = mid;
                        }

                        int ccran = gen.Next(0, komentari.Count);
                        caption_content = komentari[ccran];

                    }
                    else {
                        int atran = gen.Next(2, 7);
                        if (atran == 2 || atran == 3)
                        {
                            action_type = "liker";

                        }
                        else if (atran == 4 || atran == 5)
                        {
                            action_type = "commenter";
                            int mmc = gen.Next(0, 2);
                            if (mmc == 1)
                            {
                                int mid = gen.Next(1, multimedia_count + 1);
                                multimedia_id = mid;
                            }

                            int ccc = gen.Next(0, komentari.Count);
                            caption_content = komentari[ccc];
                        }
                        else
                        {
                            action_type = "message";
                            int uran = gen.Next(1, user_count + 1);
                            user_id_to = uran;

                            int mmu = gen.Next(0, 2);
                            if (mmu == 1)
                            {
                                int mid = gen.Next(1, multimedia_count + 1);
                                multimedia_id = mid;
                            }

                            int ccu = gen.Next(0, komentari.Count);
                            caption_content = komentari[ccu];
                        }
                    }

                    string querryPost = "(";
                    if (action_type == "message")
                    {
                        querryPost += "NULL, ";
                    }
                    else {
                        querryPost += post_id + ", ";
                    }
                    querryPost += user_id_from + ", ";
                    if (user_id_to == 0)
                    {
                        querryPost += "NULL, ";
                    }
                    else
                    {
                        querryPost += user_id_to + ", ";
                    }
                    querryPost += "'" + action_type + "', ";
                    if (group_id == 0)
                    {
                        querryPost += "NULL, ";
                    } else
                    {
                        querryPost += group_id + ", ";
                    }
                    if (multimedia_id == 0)
                    {
                        querryPost += "NULL, ";
                    } else
                    {
                        querryPost += multimedia_id + ", ";
                    }
                    querryPost += "'" + caption_content + "', ";
                    querryPost += "'" + date + "', ";
                    querryPost += active + ", ";
                    querryPost += "'" + privacy + "'),";
                    /*Console.WriteLine(querryPost);
                    Console.ReadKey();*/
                    //command.CommandText = querryPost;
                    //executeNonQuery(command);
                    unosPosts += querryPost;
                    querryPost = "(";
                }
                id++;
                post_number = 0;
                
            }
            //Console.WriteLine(unosPosts);
            string unosUFajlPost = unosPosts.Substring(0, unosPosts.Length - 1);

            System.IO.File.WriteAllText("..\\..\\..\\podaci\\postLikesCommentsMessages.txt", unosUFajlPost);
        }

        private static void fillAdvertisements(MySqlCommand command)
        {
            int client_count = clientCount(command);

            command.CommandText = "SELECT COUNT(*) FROM posts_likes_comments_messages WHERE posts_likes_comments_messages.action_type = 'poster'";
            System.Data.DataSet ds = executeSelect(command);
            int post_count = Convert.ToInt32(ds.Tables[0].Rows[0][0]);

            //Console.WriteLine("Klijenti: " + client_count);
            //Console.WriteLine("Postovi: " + post_count);



            Random gen = new Random();

            string unosAdds = "INSERT INTO advertisements VALUES ";

            for (int i = 0; i < 100; i++)
            {
                string querryAdd = "(0, ";

                bool datum_provera = false;
                string date = "";
                DateTime privremeni = DateTime.Now;

                int post_id = 0;
                int client_id = 0;
                while (datum_provera == false)
                {
                    int pran = gen.Next(1, post_count + 1);
                    post_id = pran;

                    int cran = gen.Next(1, client_count + 1);
                    client_id = cran;

                    command.CommandText = "SELECT date_created FROM clients WHERE client_id = " + client_id;
                    ds = executeSelect(command);
                    var client_date = ds.Tables[0].Rows[0][0].ToString();
                    DateTime datum = Convert.ToDateTime(client_date);

                    command.CommandText = "SELECT date_created FROM posts_likes_comments_messages WHERE post_id = " + post_id;
                    ds = executeSelect(command);
                    var post_date = ds.Tables[0].Rows[0][0].ToString();
                    DateTime post_datum = Convert.ToDateTime(post_date);


                    //Console.WriteLine(randomDate + "\n" + datum + "\n" + privremeni);
                    if (datum < post_datum)
                    {
                        privremeni = post_datum;
                        date = onlyDate(post_datum);
                        datum_provera = true;

                    }
                }

                //Console.WriteLine(date);
                //Console.WriteLine("prosao");

                
                
              


                List<string> kategorije = new List<string>(new string[]{"vacation", "animals", "babies", "food", "love", "dogs", "cats", "wild animals",
                "clothing", "fashion", "fans", "music", "TV shows", "movies", "coffee", "cartoon", "superheroes", "politics", "games", "sports" });

                string category = "";
                int caran = gen.Next(0, kategorije.Count());
                category = kategorije[caran];

                //Console.WriteLine("Privremeni datum: " + privremeni);


                int dran = gen.Next(1, 16);
                DateTime time_to_live_date = privremeni.AddDays(dran);
                string time_to_live = onlyDate(time_to_live_date);

                //Console.WriteLine("Dana: " + dran +" Novi datum: " + time_to_live_date);

                double price = dran * 4.35;



                querryAdd += client_id + ", ";
                querryAdd += "'" + date + "', ";
                querryAdd += Math.Round(price, 2) + ", ";
                querryAdd += "'" + category + "', ";
                querryAdd += post_id + ", ";
                querryAdd += "'" + time_to_live + "'),";
                //command.CommandText = querryAdd;
                //executeNonQuery(command);
                unosAdds += querryAdd;

                //Console.WriteLine(querryAdd);
                //Console.ReadKey();

            }
            string unosUFajlAdds = unosAdds.Substring(0, unosAdds.Length - 1);

            System.IO.File.WriteAllText("..\\..\\..\\podaci\\advertisements.txt", unosUFajlAdds);


        }

        public static string changeKey(string key, string[] keys) {
            string value = "";
            for (int i = 0; i < keys.Length; i++) {
                if (keys[i] == key) {
                    if (i == keys.Length - 1)
                    {
                        value = keys[0];
                    }
                    else {
                        value = keys[i + 1];
                    }
                };
            }


            return value;
        }

        public static List<Dictionary<string, string>> getListOfCities(string code, string regionName, string key)
        {

            WebRequest request = WebRequest.Create("http://battuta.medunes.net/api/city/" + code + "/search/?region=" + regionName + "&key=" + key);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();

            List<Dictionary<string, string>> listOfCities = new List<Dictionary<string, string>>();

            if (responseFromServer == "[]")
            {
                //Console.WriteLine("NEMA GRADA -->" + cities.Body + "|" + code + "|" + regionName);
                return listOfCities;
            }

            responseFromServer = responseFromServer.Replace("[{", "");
            responseFromServer = responseFromServer.Replace("}]", "");
            responseFromServer = responseFromServer.Replace("},{", "|");
            /*Console.Write(cities.Body);
            Console.ReadKey();*/

            string[] dataOfCities = responseFromServer.Split('|');
            foreach (string data in dataOfCities)
            {
                Dictionary<string, string> dictOfCities = new Dictionary<string, string>();
                foreach (string dictPodaci in data.Split(new string[] { "\",\"" }, StringSplitOptions.None))
                {
                    dictOfCities.Add(dictPodaci.Replace("\"", "").Split(':')[0], dictPodaci.Replace("\"", "").Split(':')[1]);
                }
                listOfCities.Add(dictOfCities);
            }
            return listOfCities;
        }

        public static List<Dictionary<string, string>> getListOfRegions(string code, string key)
        {
            WebRequest request = WebRequest.Create("http://battuta.medunes.net/api/region/" + code + "/all/?key=" + key);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();

            List<Dictionary<string, string>> listOfRegions = new List<Dictionary<string, string>>();

            if (responseFromServer == "[]")
            {
                //Console.WriteLine("NEMA REGIJA -->" + regions.Body + "|" + code);
                return listOfRegions;
            }

            responseFromServer = responseFromServer.Replace("[{", "");
            responseFromServer = responseFromServer.Replace("}]", "");
            responseFromServer = responseFromServer.Replace("},{", "|");

            string[] dataOfRegions = responseFromServer.Split('|');
            foreach (string data in dataOfRegions)
            {
                Dictionary<string, string> dictOfRegions = new Dictionary<string, string>();
                foreach (string dictPodaci in data.Split(new string[] { "\",\"" }, StringSplitOptions.None))
                {
                    dictOfRegions.Add(dictPodaci.Replace("\"", "").Split(':')[0], dictPodaci.Replace("\"", "").Split(':')[1]);
                }
                listOfRegions.Add(dictOfRegions);
            }

            return listOfRegions;
        }

        public static List<Dictionary<string, string>> getListOfCountries(string key)
        {
            WebRequest request = WebRequest.Create("http://battuta.medunes.net/api/country/all/?key=" + key);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();

            responseFromServer = responseFromServer.Replace("[{", "");
            responseFromServer = responseFromServer.Replace("}]", "");
            responseFromServer = responseFromServer.Replace("},{", "|");

            List<Dictionary<string, string>> listOfCountries = new List<Dictionary<string, string>>();

            string[] dataOfCountries = responseFromServer.Split('|');
            foreach (string data in dataOfCountries)
            {
                Dictionary<string, string> dictOfCountries = new Dictionary<string, string>();
                foreach (string dictPodaci in data.Split(','))
                {
                    dictOfCountries.Add(dictPodaci.Replace("\"", "").Split(':')[0], dictPodaci.Replace("\"", "").Split(':')[1]);
                }
                listOfCountries.Add(dictOfCountries);
            }

            reader.Close();
            dataStream.Close();
            response.Close();

            return listOfCountries;
        }

        private static void executeQueries(MySqlCommand command)
        {
            var upiti = new List<String>();
            string upit1 = "1. Profit u godini od dana 09.05.2018. do dana 09.05.2019";
            upiti.Add(upit1);
            string upit2 = "2. Razlika u broju registrovanih korisnika 2017, 2018 godina. I odjavljenih";
            upiti.Add(upit2);
            string upit3 = "3. Broj svih clanova grupa za kategoriju games po opadajucem redosledu";
            upiti.Add(upit3);
            string upit4 = "4. Lista klijenata koji su najvise potrosili novca u 2018. godini";
            upiti.Add(upit4);
            string upit5 = "5. Procenat korisnika po gradovima";
            upiti.Add(upit5);
            string upit6 = "6. Broj korisnika po danu u mesecu maju 2018. godine";
            upiti.Add(upit6);
            string upit7 = "7. Procentualni prikaz korisnika po polu za svaku postojecu grupu";
            upiti.Add(upit7);
            string upit8 = "8. Koliku aktivnost (like-ovi, poruke, komentari i postovi) u poslednjih mesec dana je imao ugaseni profil";
            upiti.Add(upit8);
            string upit9 = "9. Kolicina postova, likova, poruka, kometara i friend requestova koje je svaki korisnik objavio od datuma otvaranja njegovog profila";
            upiti.Add(upit9);
            string upit10 = "10. 10 prijatelja koje mozete da predlozite odredjenom korisniku kojeg mi izaberemo, na osnovu zajednickih prijatelja.(suggested friends)";
            upiti.Add(upit10);
            string upit11 = "11. Sa kojim korisnikom je odredjeni korisnik imao najvise imao razmenjenih poruka. (Da bi mu se na naslovnoj strain pojavljivalo najvise postova te osobe)";
            upiti.Add(upit11);
            string upit12 = "12. Najpopularniji razlog za napustanje mreze";
            upiti.Add(upit12);
            string upit13 = "13. Prikazati reklamu sa najvise lajkova odredjenom korisniku na osnovu kategorije kojoj vecina njegovih grupa koje prati pripadaju";
            upiti.Add(upit13);
            string upit14 = "14. Koja vrsta multimedije je najzastupljenija medju postovima korisnika";
            upiti.Add(upit14);
            string upit15 = "15. Procenat klijenata po drzavama";
            upiti.Add(upit15);
            string upit16 = "16. Prikazati kojim kategorijama najpopularnije reklame pripadaju, po opadajucem redosledu. (npr. prvih pet razlicitih)";
            upiti.Add(upit16);
            string upit17 = "17. Procenat multimedijalnih i obicnih postova kod svih korisnika";
            upiti.Add(upit17);
            string upit18 = "18. Prikazati kategoriju u okviru koje je ostvarena najveca zarada na reklamama";
            upiti.Add(upit18);
            string upit19 = "19. Na osnovu procenta reklama po drzavama prikazati kategoriju kojoj pripada vecina njih";
            upiti.Add(upit19); 
            string upit20 = "20. Predlaganje ljudi za prijatelje na osnovu trenutnog grada u kojem odabrana osoba zivi";
            upiti.Add(upit20);
            string upit21 = "21. Na osnovu zarada po klijentima prikazati ih po opadajucem redosledu";
            upiti.Add(upit21);
            string upit22 = "22. Na osnovu prosledjenih par bitnijih datuma u godini prikazati broj postavljenih reklama tog datuma opadajucim redosledom";
            upiti.Add(upit22);
            string upit23 = "23. Broj korisnika po svakom satu u danu 01.05.2018. godine";
            upiti.Add(upit23);


            command.CommandText = "SELECT SUM(price) as profit " +
                                   "FROM advertisements " +
                                   "WHERE date_created > '2018-05-09' AND date_created < '2019-05-09';" +

                                   "SELECT a.aktivni-b.aktivni as razlika_prijavljenih, c.neaktivni-d.neaktivni as razlika_neaktivnih  " +
                                   "FROM (SELECT COUNT(*) as aktivni FROM users WHERE YEAR(date_created) = '2017' and active = 1) a, " +
                                   "(SELECT COUNT(*) as aktivni FROM users WHERE YEAR(date_created) = '2018' and active = 1) b, " +
                                   "(SELECT COUNT(*) as neaktivni FROM users WHERE YEAR(date_created) = '2017' and active = 0) c, " +
                                   "(SELECT COUNT(*) as neaktivni FROM users WHERE YEAR(date_created) = '2018' and active = 0) d;" +

                                   "SELECT group_name, COUNT(*) AS broj_clanova " +
                                   "FROM groups " +
                                   "WHERE category = 'games' AND active = 1 GROUP BY group_name ORDER BY COUNT(*) DESC;" +

                                   "SELECT clients.first_name, clients.last_name, SUM(advertisements.price) as potrosen_novac " +
                                   "FROM advertisements INNER JOIN clients ON clients.client_id = advertisements.client_id " +
                                   "WHERE YEAR(advertisements.date_created) = '2018' GROUP BY clients.client_id " +
                                   "ORDER BY potrosen_novac DESC;" +

                                   "SELECT countries.name as drzava, states.name as pokrajina, cities.name as grad, (COUNT(*)*100)/a.ukupanBroj as procenat_korisnika_po_gradovima " +
                                   "FROM users " +
                                   "INNER JOIN cities ON cities.city_id=users.current_town_id " +
                                   "INNER JOIN states ON states.state_id = cities.state_id " +
                                   "INNER JOIN countries ON countries.country_code = states.country_code, " +
                                   "(SELECT COUNT(*) as ukupanBroj FROM users) a " +
                                   "GROUP BY current_town_id " +
                                   "ORDER BY current_town_id;" +

                                   "SELECT CAST(log_time.login AS DATE) AS datum, COUNT(DISTINCT log_time.user_id) " +
                                   "as broj_korisnika " +
                                   "FROM log_time " +
                                   "WHERE YEAR(log_time.login) = '2018' AND MONTH(log_time.login) = '05' " +
                                   "GROUP BY DAY(log_time.login) " +
                                   "ORDER BY datum;" +

                                   "SELECT groups.group_name AS ime_grupe, COUNT(*) as ukupan_broj_clanova, " +
                                   "(SELECT COUNT(*) " +
                                   "FROM groups " +
                                   "INNER JOIN users ON users.user_id = groups.follower_id " +
                                   "WHERE groups.group_name = ime_grupe AND users.gender_male = 1) * 100 / COUNT(*) AS procenat_muskih_clanova, " +
                                   "(SELECT COUNT(*) " +
                                   "FROM groups " +
                                   "INNER JOIN users ON users.user_id = groups.follower_id " +
                                   "WHERE groups.group_name = ime_grupe AND users.gender_male = 0) * 100 / COUNT(*) AS procenat_zenskih_clanova " +
                                   "FROM groups " +
                                   "INNER JOIN users ON users.user_id = groups.follower_id " +
                                   "GROUP BY groups.group_name " +
                                   "ORDER BY `group_name` ASC;" +

                                   "SELECT users.first_name, users.last_name, deleted_users.user_id AS trenutni, " +
                                   "(SELECT COUNT(*) " +
                                   "FROM posts_likes_comments_messages " +
                                   "INNER JOIN deleted_users ON deleted_users.user_id = posts_likes_comments_messages.user_id_from " +
                                   "WHERE posts_likes_comments_messages.user_id_from = trenutni AND posts_likes_comments_messages.action_type = 'liker' " +
                                   "AND posts_likes_comments_messages.date_created > DATE_SUB(deleted_users.date_deleted, INTERVAL 1 MONTH) " +
                                   "AND posts_likes_comments_messages.date_created < deleted_users.date_deleted) " +
                                   "AS broj_likova, " +
                                   "(SELECT COUNT(*) " +
                                   "FROM posts_likes_comments_messages " +
                                   "WHERE posts_likes_comments_messages.user_id_from = trenutni AND posts_likes_comments_messages.action_type = 'commenter' " +
                                   "AND posts_likes_comments_messages.date_created > DATE_SUB(deleted_users.date_deleted, INTERVAL 1 MONTH) " +
                                   "AND posts_likes_comments_messages.date_created < deleted_users.date_deleted) " +
                                   "AS broj_komentara, (SELECT COUNT(*) FROM posts_likes_comments_messages WHERE posts_likes_comments_messages.user_id_from = trenutni AND posts_likes_comments_messages.action_type = 'message' AND posts_likes_comments_messages.date_created > DATE_SUB(deleted_users.date_deleted, INTERVAL 1 MONTH) AND  posts_likes_comments_messages.date_created < deleted_users.date_deleted" +
                                   ") AS broj_poruka, (SELECT COUNT(*) FROM posts_likes_comments_messages WHERE posts_likes_comments_messages.user_id_from = trenutni AND posts_likes_comments_messages.action_type = 'poster' AND posts_likes_comments_messages.date_created > DATE_SUB(deleted_users.date_deleted, INTERVAL 1 MONTH) AND  posts_likes_comments_messages.date_created < deleted_users.date_deleted" +
                                   ") AS broj_postova FROM deleted_users INNER JOIN users ON users.user_id = deleted_users.user_id GROUP BY deleted_users.user_id;" +

                                   "SELECT users.first_name, users.last_name, users.user_id AS id, (SELECT COUNT(*) FROM friend_requests WHERE friend_requests.user_id_from = id) AS broj_friend_requestova, (SELECT COUNT(*) FROM posts_likes_comments_messages WHERE posts_likes_comments_messages.user_id_from = id AND posts_likes_comments_messages.action_type = 'liker') AS broj_likova, (SELECT COUNT(*) FROM posts_likes_comments_messages WHERE posts_likes_comments_messages.user_id_from = id AND posts_likes_comments_messages.action_type = 'commenter') AS broj_komentara, (SELECT COUNT(*) FROM posts_likes_comments_messages WHERE posts_likes_comments_messages.user_id_from = id AND posts_likes_comments_messages.action_type = 'poster') AS broj_postova, (SELECT COUNT(*) FROM posts_likes_comments_messages WHERE posts_likes_comments_messages.user_id_from = id AND posts_likes_comments_messages.action_type = 'message') AS broj_poruka FROM users GROUP BY users.user_id;" +
                                   "SELECT friends.friend_user_id, users.first_name, users.last_name, COUNT(*) AS zajednicki_prijatelji FROM friends INNER JOIN users ON users.user_id = friends.friend_user_id, (SELECT * FROM friends WHERE friends.user_id = 1) friends_of_selected WHERE friends.user_id = friends_of_selected.friend_user_id AND friends.friend_user_id <> 1 AND friends.friend_user_id NOT IN (SELECT friends.friend_user_id FROM friends WHERE friends.user_id = 1) GROUP BY friends.friend_user_id ORDER BY zajednicki_prijatelji DESC LIMIT 10;" +
                                   "SELECT users.first_name, users.last_name, posts_likes_comments_messages.user_id_from AS id_osobe, (SELECT posts_likes_comments_messages.user_id_to FROM posts_likes_comments_messages WHERE posts_likes_comments_messages.user_id_from = id_osobe AND posts_likes_comments_messages.action_type = 'message' GROUP BY posts_likes_comments_messages.user_id_to ORDER BY COUNT(*) LIMIT 1) AS id FROM posts_likes_comments_messages INNER JOIN users ON users.user_id = posts_likes_comments_messages.user_id_from GROUP BY posts_likes_comments_messages.user_id_from;" +
                                   "SELECT deleted_users.reason, COUNT(*) as broj_odgovora FROM deleted_users GROUP BY deleted_users.reason ORDER BY broj_odgovora DESC;" +
                                   "SELECT posts_likes_comments_messages.post_id, COUNT(*) AS broj_likeova FROM posts_likes_comments_messages, (SELECT advertisements.post_id FROM advertisements, (SELECT groups.category FROM groups WHERE groups.follower_id = 46) kategorija WHERE advertisements.category = kategorija.category) post WHERE posts_likes_comments_messages.post_id = post.post_id AND posts_likes_comments_messages.action_type = 'liker' GROUP BY posts_likes_comments_messages.post_id ORDER BY broj_likeova DESC;" +
                                   "SELECT multimedia.type FROM multimedia, (SELECT posts_likes_comments_messages.multimedia_id AS id FROM posts_likes_comments_messages WHERE posts_likes_comments_messages.action_type = 'poster' AND posts_likes_comments_messages.multimedia_id IS NOT NULL GROUP BY posts_likes_comments_messages.multimedia_id ORDER BY COUNT(*) DESC LIMIT 1) AS najpopularniji_medij WHERE multimedia.multimedia_id = id;" +
                                   "SELECT countries.name, countries.country_code, COUNT(*) broj_klijenata, COUNT(*)*100/(SELECT COUNT(*) FROM clients) AS procenat_klijenata FROM clients INNER JOIN cities ON cities.city_id = clients.city_id INNER JOIN states ON states.state_id = cities.state_id INNER JOIN countries ON countries.country_code = states.country_code GROUP BY countries.country_code;" +
                                   "SELECT advertisements.category, SUM(reklame.popularnost) as broj_reklama_u_kategoriji FROM advertisements, (SELECT posts_likes_comments_messages.post_id as id,COUNT(*) as popularnost FROM posts_likes_comments_messages, (SELECT advertisements.post_id FROM advertisements) post WHERE posts_likes_comments_messages.post_id = post.post_id AND posts_likes_comments_messages.action_type = 'liker' GROUP BY posts_likes_comments_messages.post_id ORDER BY popularnost DESC) reklame WHERE advertisements.post_id = reklame.id GROUP BY advertisements.category ORDER BY broj_reklama_u_kategoriji DESC;" +
                                   "SELECT posts_likes_comments_messages.user_id_from AS id_osobe, COUNT(*) ukupan_broj_postova, (SELECT COUNT(*) FROM posts_likes_comments_messages WHERE posts_likes_comments_messages.action_type = 'poster' AND posts_likes_comments_messages.user_id_from = id_osobe AND posts_likes_comments_messages.multimedia_id IS NOT NULL)*100/COUNT(*) AS procenat_multimedijalnih_postova, (SELECT COUNT(*) FROM posts_likes_comments_messages WHERE posts_likes_comments_messages.action_type = 'poster' AND posts_likes_comments_messages.user_id_from = id_osobe AND posts_likes_comments_messages.multimedia_id IS NULL)*100/COUNT(*) AS procenat_obicnih_postova FROM posts_likes_comments_messages WHERE posts_likes_comments_messages.action_type = 'poster' GROUP BY posts_likes_comments_messages.user_id_from;" +
                                   "SELECT advertisements.category, SUM(advertisements.price) AS zarada_po_kategoriji FROM advertisements GROUP BY advertisements.category ORDER BY zarada_po_kategoriji DESC;" +
                                   "SELECT countries.name, countries.country_code AS trenutna_drzava, (SELECT advertisements.category FROM advertisements INNER JOIN clients ON clients.client_id = advertisements.client_id INNER JOIN cities ON cities.city_id = clients.city_id INNER JOIN states ON states.state_id = cities.state_id INNER JOIN countries ON countries.country_code = states.country_code WHERE countries.country_code = trenutna_drzava GROUP BY advertisements.category ORDER BY COUNT(*) DESC LIMIT 1) kategorija FROM advertisements INNER JOIN clients ON clients.client_id = advertisements.client_id INNER JOIN cities ON cities.city_id = clients.city_id INNER JOIN states ON states.state_id = cities.state_id INNER JOIN countries ON countries.country_code = states.country_code GROUP BY countries.country_code;" +
                                   "SELECT users.first_name, users.last_name FROM users, (SELECT users.current_town_id FROM users WHERE users.user_id = 17) odabrani WHERE users.current_town_id = odabrani.current_town_id;" +
                                   "SELECT clients.client_id, clients.first_name, clients.last_name, SUM(advertisements.price) as ukupno_potrosen_novac FROM advertisements INNER JOIN clients ON clients.client_id = advertisements.client_id GROUP BY clients.client_id ORDER BY ukupno_potrosen_novac DESC;" +
                                   "SELECT advertisements.date_created, advertisements.category, COUNT(*) AS postavljeno_reklama FROM advertisements WHERE advertisements.date_created = '2018-11-14' OR advertisements.date_created = '2018-05-17' OR advertisements.date_created = '2017-05-29' GROUP BY advertisements.date_created ORDER BY postavljeno_reklama DESC;" +

                                   "SELECT CAST(log_time.login AS DATE) AS datum, HOUR(log_time.login) AS sat, COUNT(DISTINCT log_time.user_id) AS broj_korisnika " +
                                   "FROM log_time " +
                                   "WHERE YEAR(log_time.login) = '2018' AND MONTH(log_time.login) = '05' AND DAY(log_time.login) = '01' " +
                                   "GROUP BY HOUR(log_time.login) " +
                                   "ORDER BY sat;";
            System.Data.DataSet ubb = executeSelect(command);

            for (int k = 0; k < ubb.Tables.Count; k++)
            {
                PrintLine();
                List<string> naziv_kolona = new List<string>();
                List<List<string>> redovi = new List<List<string>>();
                for (int i = 0; i < ubb.Tables[k].Rows.Count; i++)
                {
                    List<string> red = new List<string>();
                    for (int j = 0; j < ubb.Tables[k].Columns.Count; j++)
                    {
                        if (i == 0)
                        {
                            naziv_kolona.Add(ubb.Tables[k].Columns[j].ColumnName);
                        }
                        red.Add(ubb.Tables[k].Rows[i][j].ToString());
                    }
                    redovi.Add(red);
                }
                PrintRow(upiti[k]);
                PrintLine();
                PrintRow(naziv_kolona.ToArray());
                PrintLine();
                foreach (List<string> red in redovi)
                {
                    PrintRow(red.ToArray());
                }
                PrintLine();
                Console.WriteLine();
                Console.WriteLine();
            }

            /*for (int k = 0; k < ubb.Tables.Count; k++) {
                string tabela = upiti[k] + ": \n";
                string naziv_kolona = "";
                string redovi = "";
                for (int i = 0; i < ubb.Tables[k].Rows.Count; i++) { 
                    string red = "";
                    for (int j = 0; j < ubb.Tables[k].Columns.Count; j++) {
                        if (i == 0) {
                            naziv_kolona += ubb.Tables[k].Columns[j].ColumnName + " | ";
                        }
                        red += ubb.Tables[k].Rows[i][j] + ", ";
                    }
                    //Console.WriteLine(red);
                    redovi += red + "\n";
                }
                Console.WriteLine(tabela + naziv_kolona + "\n" + redovi + "\n");
            }*/
        }

        private static void createTables(MySqlCommand command)
        {
            command.CommandText = "CREATE TABLE IF NOT EXISTS countries ("
                            + "country_code             VARCHAR(50) PRIMARY KEY NOT NULL,"
                            + "name                     VARCHAR(255) NOT NULL UNIQUE) ENGINE = InnoDB";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS states("
                            + "state_id                 INT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                            + "name                     VARCHAR(255) NOT NULL,"
                            + "country_code             VARCHAR(50) NOT NULL,"
                            + "FOREIGN KEY(country_code) REFERENCES countries(country_code)) ENGINE = InnoDB";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS cities ("
                            + "city_id                  INT(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                            + "name                     VARCHAR(255) NOT NULL,"
                            + "state_id                 INT(20) NULL,"
                            + "FOREIGN KEY(state_id) REFERENCES states(state_id)) ENGINE = InnoDB";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS multimedia("
                            + "multimedia_id            INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                            + "type                     ENUM('image', 'video', 'sound', 'link') NOT NULL DEFAULT 'image',"
                            + "path                     VARCHAR(255) NOT NULL) ENGINE = InnoDB; ";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS users ("
                            + "user_id                  INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                            + "username                 VARCHAR(255) NOT NULL UNIQUE,"
                            + "password                 VARCHAR(255) NOT NULL,"
                            + "email                    VARCHAR(255) NOT NULL UNIQUE,"
                            + "first_name               VARCHAR(255) NOT NULL,"
                            + "last_name                VARCHAR(255) NOT NULL,"
                            + "birthday                 DATE NOT NULL,"
                            + "gender_male              BOOLEAN NOT NULL DEFAULT true,"
                            + "date_created             DATE NOT NULL,"
                            + "date_updated             DATE NULL,"
                            + "active                   BOOLEAN NOT NULL DEFAULT true,"
                            + "hometown_id              INT NOT NULL,"
                            + "current_town_id          INT NOT NULL,"
                            + "FOREIGN KEY(hometown_id) REFERENCES cities(city_id),"
                            + "FOREIGN KEY(current_town_id) REFERENCES cities(city_id)) ENGINE = InnoDB";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS groups("
                            + "group_id                 INT NOT NULL,"
                            + "group_name               VARCHAR(255) NOT NULL,"
                            + "date_crated              DATE NOT NULL,"
                            + "active                   BOOLEAN NOT NULL DEFAULT true,"
                            + "type                     ENUM('private', 'public') NOT NULL DEFAULT 'public',"
                            + "info                     TEXT NULL,"
                            + "category                 ENUM ('vacation', 'animals', 'babies', 'food', 'love', 'dogs', 'cats', 'wild animals', 'clothing', 'fashion', 'fans', 'music', 'TV shows', 'movies', 'coffee', 'cartoon', 'superheroes', 'politics', 'games', 'sports') NOT NULL DEFAULT 'fans',"
                            + "follower_id              INT NULL,"
                            + "membership_type          ENUM ('admin', 'member', 'creator') NOT NULL DEFAULT 'member',"
                            + "FOREIGN KEY(follower_id)REFERENCES users(user_id)) ENGINE = InnoDB";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS posts_likes_comments_messages ("
                            + "post_id                  INT NULL,"
                            + "user_id_from             INT NOT NULL,"
                            + "user_id_to               INT NULL,"
                            + "action_type              ENUM ('poster', 'liker', 'commenter', 'message') NOT NULL DEFAULT 'poster',"
                            + "group_id                 INT NULL,"
                            + "multimedia_id            INT NULL,"
                            + "caption_content          TEXT NULL,"
                            + "date_created             DATE NOT NULL,"
                            + "active                   BOOLEAN NOT NULL DEFAULT true,"
                            + "privacy                  ENUM ('public', 'private') NOT NULL DEFAULT 'public',"
                            + "FOREIGN KEY(user_id_from)REFERENCES users(user_id),"
                            + "FOREIGN KEY(user_id_to) REFERENCES users(user_id),"
                            + "FOREIGN KEY(multimedia_id) REFERENCES multimedia(multimedia_id)) ENGINE = InnoDB";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS friend_requests ("
                            + "user_id_from             INT NOT NULL,"
                            + "user_id_to               INT NOT NULL,"
                            + "status                   ENUM ('sent', 'rejected', 'friends') NOT NULL DEFAULT 'sent',"
                            + "date_created             DATE NOT NULL,"
                            + "PRIMARY KEY(user_id_from, user_id_to),"
                            + "FOREIGN KEY(user_id_from)REFERENCES users(user_id),"
                            + "FOREIGN KEY(user_id_to) REFERENCES users(user_id)) ENGINE = InnoDB";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS friends("
                            + "user_id                  INT NOT NULL,"
                            + "friend_user_id           INT NOT NULL,"
                            + "PRIMARY KEY(user_id, friend_user_id),"
                            + "FOREIGN KEY(user_id) REFERENCES users(user_id),"
                            + "FOREIGN KEY(friend_user_id) REFERENCES users(user_id)) ENGINE = InnoDB";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS clients ("
                            + "client_id                INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
                            + "first_name               VARCHAR(255) NOT NULL,"
                            + "last_name                VARCHAR(255) NOT NULL,"
                            + "billing_address          VARCHAR(255) NOT NULL,"
                            + "city_id                  INT NOT NULL,"
                            + "credit_card_number       INT NOT NULL,"
                            + "date_created             DATE NOT NULL,"
                            + "FOREIGN KEY(city_id) REFERENCES cities(city_id)) ENGINE = InnoDB";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS advertisements("
                            + "advertisement_id         INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
                            + "client_id                INT NOT NULL,"
                            + "date_created             DATE NOT NULL,"
                            + "price                    DOUBLE NOT NULL,"
                            + "category                 ENUM('vacation', 'animals', 'babies', 'food', 'love', 'dogs', 'cats', 'wild animals', 'clothing', 'fashion', 'fans', 'music', 'TV shows', 'movies', 'coffee', 'cartoon', 'superheroes', 'politics', 'games', 'sports') NOT NULL DEFAULT 'fans',"
                            + "post_id                  INT NOT NULL,"
                            + "time_to_live             DATE NOT NULL,"
                            + "FOREIGN KEY(client_id)REFERENCES clients(client_id)) ENGINE = InnoDB";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS log_time("
                            + "log_id                   INT NOT NULL PRIMARY KEY AUTO_INCREMENT,"
                            + "user_id                  INT NOT NULL,"
                            + "login                    DATETIME NOT NULL,"
                            + "logout                   DATETIME NULL,"
                            + "FOREIGN KEY(user_id)REFERENCES users(user_id)) ENGINE = InnoDB";
            executeNonQuery(command);

            command.CommandText = "CREATE TABLE IF NOT EXISTS deleted_users("
                            + "user_id                  INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                            + "date_deleted             DATE NOT NULL,"
                            + "reason                   ENUM(\"Other\", \"I don't like this anymore\", \"Found something better\", \"Too much technical problems\", \"Personal reasons\", \"Deleted by admin\") NOT NULL DEFAULT 'Other',"
                            + "FOREIGN KEY(user_id)REFERENCES users(user_id)) ENGINE = InnoDB";
            executeNonQuery(command);

        }

        public static string dtfordb(DateTime dt)
        {
            return dt.Year + "-" + dt.Month + "-" + dt.Day + " " + dt.TimeOfDay;
        }

        public static string onlyDate(DateTime dt)
        {
            return dt.Year + "-" + dt.Month + "-" + dt.Day;
        }

        public static void executeNonQuery(MySqlCommand commandsw)
        {
            bool b = false;

            while (!b)
            {
                try
                {
                    if (!commandsw.Connection.Ping() || commandsw.Connection.State == System.Data.ConnectionState.Closed)
                    {
                        commandsw.Connection.Open();
                    }
                    commandsw.ExecuteNonQuery();
                    b = true;
                }
                catch
                {
                    Console.WriteLine(DateTime.Now + "Vrtimo se u petlji upisa u bazu");
                    Console.WriteLine(DateTime.Now + " " + commandsw.CommandText);
                    b = false;
                }
            }
        }

        public static System.Data.DataSet executeSelect(MySqlCommand command)
        {
            bool b = false;
            System.Data.DataSet cas = new System.Data.DataSet();

            while (!b)
            {
                try
                {
                    if (!command.Connection.Ping() || command.Connection.State == System.Data.ConnectionState.Closed)
                    {
                        command.Connection.Open();
                    }
                    MySqlDataAdapter adapter = new MySqlDataAdapter(command.CommandText, command.Connection);
                    adapter.Fill(cas);
                    b = true;
                }
                catch
                {
                    Console.WriteLine(DateTime.Now + "Vrtimo se u petlji SELECT u bazi");
                    Console.WriteLine(DateTime.Now + " " + command.CommandText);
                    b = false;
                }
            }
            return cas;
        }
    }
}
